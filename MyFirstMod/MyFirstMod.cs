﻿using System;
using BepInEx;
using BepInEx.Configuration;

namespace MyFirstMod
{
    [BepInPlugin("org.bepinex.plugins.myfirstmod", "MyFirstMod", "0.0.1")]
    [BepInProcess("valheim.exe")]
    public class MyFirstMod : BaseUnityPlugin
    {
        private ConfigEntry<string> configGreeting;
        private ConfigEntry<bool> configDisplayGreeting;

        // Awake is called once when both the game and the plug-in are loaded
        void Awake()
        {
            configGreeting = Config.Bind("General",   // The section under which the option is shown
                                         "GreetingText",  // The key of the configuration option in the configuration file
                                         "CHANGE ME IN CONFIG", // The default value
                                         "A greeting text to show when the game is launched"); // Description of the option to show in the config file

            configDisplayGreeting = Config.Bind("General.Toggles",
                                                "DisplayGreeting",
                                                true,
                                                "Whether or not to show the greeting text");
            
            // Instead of just hard coding the value read from config
            // Logger.LogInf0("ALL YOUR BASE NOW BELONG TO US")
            if (configDisplayGreeting.Value)
                Logger.LogInfo(configGreeting.Value);
        }
    }
}
